﻿// Создайте 3 произвольных фигуры разной формы, цвета и размера. Сделайте так, 
// чтобы они из своих исходных позиций перемещались вниз экрана. При достижении низа экрана 
// фигуры должны остановиться.

#include <SFML/Graphics.hpp>

int main()
{
    float x_circle = 100;
    float y_circle = 100;
    float x_rectangle = 128;
    float y_rectangle = 65;
    float x_rhombus = 80;
    float y_rhombus = 80;

    sf::RenderWindow window(sf::VideoMode(1000, 1000), L"Игра - упади первым!");
    sf::CircleShape circle(x_circle);
    circle.setPosition(125.f, y_circle);
    circle.setOrigin(x_circle, y_circle);
    circle.setFillColor(sf::Color::Green);

    sf::RectangleShape rectangle(sf::Vector2f(x_rectangle*2, y_rectangle*2));
    rectangle.setPosition(500.f, y_rectangle);
    rectangle.setOrigin(x_rectangle, y_rectangle);
    rectangle.setFillColor(sf::Color::Blue);

    sf::CircleShape rhombus(x_rhombus, 4.f);
    rhombus.setPosition(900.f, y_rhombus);
    rhombus.setOrigin(x_rhombus, y_rhombus);
    rhombus.setFillColor(sf::Color::Red);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        if (y_circle < 900)
            y_circle++;

        if (y_rectangle < 935)
            y_rectangle++;

        if (y_rhombus < 920)
            y_rhombus++;

        circle.setPosition(125.f, y_circle);
        rectangle.setPosition(500.f, y_rectangle);
        rhombus.setPosition(900.f, y_rhombus);
        window.clear();
        window.draw(circle);
        window.draw(rectangle);
        window.draw(rhombus);
        window.display();
    }

    return 0;
}